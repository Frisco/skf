<?php

/**
 * File containing the index for system.
 *
 * @package SKF
 * @copyright Copyright (C) 2019 PHPRO.ORG. All rights reserved.
 * @filesource
 *
 */

namespace skf;

session_start();
/*
	// define the site path
	$site_path = realpath(dirname(__FILE__));
	define ('SITE_PATH', $site_path);

	// the application directory path 
	define ('APP_PATH', SITE_PATH.'/application');

	// add the application to the include path
	set_include_path( APP_PATH );
	set_include_path( SITE_PATH );

	// set the public web root path
	$path = str_replace($_SERVER['DOCUMENT_ROOT'], '', SITE_PATH);
	define('PUBLIC_PATH', $path);

	spl_autoload_register(null, false);

	spl_autoload_extensions('.php, .class.php, .lang.php');
 */

try {
	include 'init/init.php';

	// set the domain status
	// $domain_config = domain_config::getInstance();
	// var_dump( $domain_config );

	// set the timezone
	date_default_timezone_set($config->config_values['application']['timezone']);

	/**
	 *
	 * @custom error function to throw exception
	 *
	 * @param int $errno The error number
	 *
	 * @param string $errmsg The error message
	 *
	 */
	function skfErrorHandler($errno, $errmsg)
	{
		throw new skfException($errmsg, $errno);
	}
	/*** set error handler level to E_WARNING ***/
	// set_error_handler('skfErrorHandler', $config->config_values['application']['error_reporting']);

	// Initialize the FrontController
	// $front = new FrontController;
	$front = (new Resolver)->resolve( 'skf\FrontController' );
	$front->route();
 	echo $front;
}
catch(skfException $e)
{
	throw new \Exception ($e);
}
// catch exceptions from the php exception class
catch( \Exception $e )
{
	// if we are here, we are the top of pond for exceptions to bubble up
	// here we send off to a page for system errors
	$front = (new Resolver)->resolve( 'skf\FrontController' );
	include_once APP_PATH.'/models/view.php';
	$tpl = new view;
	$tpl->setTemplateDir( APP_PATH."/layouts" );
	$tpl->error_message = $e->getMessage();
	$tpl->error_line = $e->getLine();
	$tpl->error_file = $e->getFile();
	$tpl->backtrace = $e->getTraceAsString();
	$tpl->version = $config->config_values['application']['version'];
	$tpl->title = 'System Error!';
	$tpl->content = $tpl->getOutput( APP_PATH.'/modules/error500/views/index.phtml' );
	$tpl->render( "index.phtml" );
}
