# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The SKF is a simple PHP framework for build websites, applications, and APIs. 
* 0.9
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* SKF Framework is designed to work out-of-the box. A number of Zurb Foundation templates has been included to get you started. 
* Configuration for database and other options may be done in the application/config/config.ini.php file
* There are no external dependencies for the SKF framework. It is designed to work as a stand-alone application or site.
* Deployment is simple, just check out from this repository, and deploy into you web directory.

### Documentation ###

* Docs can be found in the docs module /application/modules/docs
* Docs URL is /docs if you have SKF set up

### Who do I talk to? ###

* Repo owner or admin
