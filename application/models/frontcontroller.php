<?php

/**
 *
 * @Front Controller class
 *
 * @copyright Copyright (C) 2019 PHPRO.ORG. All rights reserved.
 *
 * @license new bsd http://www.opensource.org/licenses/bsd-license.php
 * @package Core
 * @Author Kevin Waterson
 *
 */

namespace skf;

class FrontController
{

	protected $_controller, $_action, $_params, $_body, $_url;

	/**
	 * Constructor, sets up the controller and action
	 *
	 **/
	public function __construct( View $view, Uri $uri, Config $config )
	{
		// set the controller

		$this->_uri = $uri;
		$this->view = $view;
		$this->config = $config;

		/*
		if( $this->_uri->fragment(0) && $this->_uri->fragment(0)=='assets' )
		{
			exit;
		}
		*/

		if( $this->_uri->fragment(0) )
		{
			$this->_controller = $this->_uri->fragment(0).'Controller';
		}
		else
		{
			// get the default controller
			// $config = new config;
			$default = $config->config_values['application']['default_controller'].'Controller';
			$this->_controller = $default;
		}

		// the action
		if( $this->_uri->fragment(1) )
		{
			// if fragment(1) is admin, then we load the admin controller
			if( $this->_uri->fragment(1) == 'admin' )
			{
				$admin_controller = $this->_uri->fragment(0).'_adminController';
				$this->_controller = $admin_controller;
				$this->_action = $this->_uri->fragment(2);
			}
			else
			{
				$this->_action = 'call'.$this->_uri->fragment(1);
			}
		}
		else
		{
			$this->_action = 'callIndex';
		}
	}

	/**
	 *
 	 * The route
	 *
	 * Checks if controller and action exists
	 *
	 * @access	public
	 *
 	 */
	public function route()
	{
		// check if the controller exists
		$con = $this->getController();

		$rc = new \ReflectionClass( "skf\\$con" );
		// if the controller exists and implements IController
		if( $rc->implementsInterface( 'skf\IController' ) )
		{
			$controller = (new Resolver)->resolve( "skf\\$con" );
			// $controller = $rc->newInstance( $this->view, $this->_uri, $this->config );
			// check if method exists 
			if( $rc->hasMethod( $this->getAction() ) )
			{
				// if all is well, load the action
				$method = $rc->getMethod( $this->getAction() );
			}
			else
			{
				// load the default action method
				$default = $this->config->config_values['application']['default_action'];
				$method = $rc->getMethod( $default );
			}
			// run the controller eg: blog::index()
			$method->invoke( $controller );

			// now set the page in the basecontroller
			$method = $rc->getMethod( 'setPage' );
			$method->invoke( $controller );

			$method = $rc->getMethod( 'getPage' );
			$res = $method->invoke( $controller );
			$this->setBody( $res );
		}
		else
		{
			throw new \Exception("Interface iController must be implemented");
		}
	}

	/*
	public function getParams()
	{
		return $this->_params;
	}
	*/

	/**
	*
	* Gets the controller, sets to default if not available
	*
	* @access	public
	* @return	string	The name of the controller
	*
	*/
	public function getController()
	{
		if( class_exists( 'skf\\'.$this->_controller ) )
		{
			return $this->_controller;
		}
		else
		{
			$default = $this->config->config_values['application']['error_controller'].'Controller';
			return $default;
		}
	}

	/**
	*
	* Get the action
	*
	* @access	public
	* @return	string	the Name of the action
	*
	*/
	public function getAction()
	{
		return $this->_action;
	}

	public function getBody()
	{
		return $this->_body;
	}

	public function setBody( $body )
	{
		$this->_body = $body;
	}


	public function __toString()
	{
		return $this->getBody();
	}
} // end of class
