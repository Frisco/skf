<?php

namespace skf;

class validate_skeleton extends \skf\validation{

        public function loadRules()
	{
		$err = "Module name is invalid. Please think of the children";
                $this->addValidator( ['name'=>'module_name', 'type'=>'string', 'required'=>true, 'min'=>1, 'max'=>80, 'trim'=>1] );
        }

} // end of class

