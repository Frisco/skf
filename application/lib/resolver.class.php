<?php

namespace skf;

class resolver {

	/**
 	* Build an instance of the given class
	* 
 	* @param string $class
 	* @return mixed
 	*
 	* @throws Exception
 	*/
	public function resolve( $class )
	{
		$reflector = new \ReflectionClass( $class );

		// if we cannot instantiate the class
		if( ! $reflector->isInstantiable())
 		{
 			throw new \Exception( "[$class] is not instantiable" );
 		}

		// fetch the constructor	
 		$constructor = $reflector->getConstructor();
		// if we do not have a constructor..
 		if( is_null( $constructor ) )
 		{
 			return new $class;
 		}
		// fetch parameters
		$parameters = $constructor->getParameters();

		// fetch the dependencies based on the parameters
		$dependencies = $this->getDependencies( $parameters );

 		return $reflector->newInstanceArgs( $dependencies );
	}
	
	/**
	 * Build up a list of dependencies for a given methods parameters
	 *
	 * @param array $parameters
	 * @return array
	 */
	public function getDependencies( array $parameters ):array
	{
		// dependency container
		$dependencies = [];
		
		// loop through all parameters to find classes
		foreach( $parameters as $parameter )
		{
			$dependency = $parameter->getClass();
			
			if( is_null( $dependency ) )
			{
				$dependencies[] = $this->resolveNonClass( $parameter );
			}
			else
			{
				$dependencies[] = $this->resolve( $dependency->name );
			}
		}
		return $dependencies;
	}
	
	/**
	 * Determine what to do with a non-class value
	 *
	 * @param ReflectionParameter $parameter
	 * @return mixed
	 *
	 * @throws Exception
	 */
	public function resolveNonClass( \ReflectionParameter $parameter )
	{
		// If we have a constructor, we will need to loop through all 
		// of the parameters and determine if each parameter is a resolvable class. 
		// If it is, run it back through our initial “resolve” method allowing us to 
		// indefinately nest our automatic class resoultion. 
		if( $parameter->isDefaultValueAvailable() )
		{
			return $parameter->getDefaultValue();
		}
		// If the parameter is not a class, and no default value was supplied 
		// we have nothing left to do but throw an Exception
		// throw new \Exception( "$parameter: A suffusion of yellow" );
		return false;
	}
} // end of class

?>
