<?php

namespace skf;

class form extends \domDocument{

	/**
	 * Constructor
	 *
	 * Creates the form
	 * @access	public
	 *
	 */
	public function __construct()
	{
	}

	public function create( $name='form' )
	{
		parent::__construct();
		$this->formatOutput = true;
		$form = $this->createElement( $name );
		$this->appendChild( $form );
	}

	/**
	 * Settor
	 *
	 * @access	public
	 * @param	string	$name
	 * @param	mixed	$value
	 * @return	void
	 *
	 */
	public function __set( string $name, $value=null )
	{
		$att = $this->createAttribute( $name );
		$att->value = $value;
		$this->documentElement->appendChild( $att );
	}

	/**
	 * Gettor
	 *
	 * @access	public
	 * @param	string	$name
	 * @return	string
	 *
	 */
	public function __get( string $name )
	{
		return $this->$name;
	}

	public function formGen()
	{
		foreach( $this->elements as $el )
		{
			$el = $this->importNode( $el->documentElement, true );
			$this->documentElement->appendChild( $el );
		}
	}

	/**
	 * Add a field to this object
	 *
	 * @access	public
	 * @param	domDocument	$field
	 * @return	void
	 *
	 */
	public function addField( \domDocument $field )
	{
		$field = $this->importNode( $field->documentElement, true );
		$this->documentElement->appendChild( $field );
	}

	/**
	 * Returns a string representation of this class
	 *
	 * @access	public
	 * @return	string
	 *
	 */
	public function __toString():string
	{
		return $this->saveHTML();
	}
}

?>
