<?php

namespace skf;

class validate_builddao extends \skf\validation{

        public function loadRules()
        {
                $this->addValidator( array( 'name'=>'table_name', 'type'=>'string', 'required'=>false, 'min'=>1, 'max'=>80, 'trim'=>1 ) );
        }

} // end of class

