<?php

namespace skf;

class dao_migrations extends dao{

	public $primary_key = 'id';
	public $id;
	public $filename;
	public $checksum;
	public $last_updated;

	public function __construct( Db $db ){
		parent::__construct( $db );
	}


		#################################################
		### CUSTOM FUNCTIONS MUST GO BELOW THIS BLOCK ###
		#################################################

	/**
	 * Check if a file has been previously loaded
	 * 
	 * @access	public
	 * @param	string	$filepath
	 * @return	bool	true if has been previously loaded, false otherwise
	 *
	 */
	public function isLoaded( $filepath )
	{
		$checksum = md5( file_get_contents( $filepath ) );
		$sql = "SELECT id FROM migrations WHERE checksum=:checksum";
		$stmt = $this->db->prepare( $sql );
		$stmt->bindParam( ':checksum', $checksum, \PDO::PARAM_STR );
		$stmt->execute();
		$num_rows = $stmt->fetchAll( \PDO::FETCH_COLUMN, 0 );
		return count( $num_rows ) > 0;
	}

	/**
	 * Load a migration file into the database
	 *
	 * @access      public
	 * @param       string  $filepath
	 * @return      void
	 *
	 */
	public function loadMigration( $filepath )
	{
		$sql = file_get_contents( $filepath );
		$checksum = md5( file_get_contents( $filepath ) );
		try{
			$this->db->beginTransaction();
			$this->db->exec( $sql );
			$this->incrementMigration( $filepath, $checksum );
			$this->db->commit();
			return "Success: File $filepath loaded successfully.";
		}
		catch( \PDOException $e )
		{
			$this->db->rollBack();
		}
	}


	/**
	 * Check if a migration has been modified since import
	 *
	 * @access	private
	 * @param	string	$filepath
	 * @return	bool	(true if modified, false otherwise)
	 *
	 */
	public function isModified( $filepath )
	{
		$filename = basename( $filepath );
		$file_checksum = md5( file_get_contents( $filepath ) );

		$sql = "SELECT checksum FROM migrations WHERE filename=:filename";
		$stmt = $this->db->prepare( $sql );
		$stmt->bindParam( ':filename', $filename, \PDO::PARAM_STR );
		$stmt->execute();
		$res = $stmt->fetchAll( \PDO::FETCH_COLUMN, 0 );
		// if a file is not yet in db, there will be zero results, so..
		if( sizeof( $res ) == 1 )
		{
			$stored_checksum = $res[0];
			// if not same, file has been altered, return true.
			if( $file_checksum != $stored_checksum )
			{
				// File has been modified since migration
				return true;
			}
			return false;
		}
		return false;
	}


	/**
	 * Add files to migrations table
	 *
	 * @access	private
	 * @param	string	$filename
	 * @param	string	$checksum
	 * @return	void
	 *
	 */
	private function incrementMigration( string $filename, string $checksum )
	{
		$sql = "INSERT INTO migrations (filename, checksum) VALUES (:filename, :checksum)";
		$stmt = $this->db->prepare( $sql );
		$stmt->bindParam( ':filename', $filename, \PDO::PARAM_STR );
		$stmt->bindParam( ':checksum', $checksum, \PDO::PARAM_STR );
		$stmt->execute();
	}


} // end of class
?>
