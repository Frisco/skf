<?php

namespace skf;

class dao_blog extends \skf\dao{

	public $primary_key = 'id';
	public $id;
	public $title;
	public $body;
	public $category_id;
	public $last_updated;

	public function __construct( Db $db ){
		parent::__construct( $db );
	}


		#################################################
		### CUSTOM FUNCTIONS MUST GO BELOW THIS BLOCK ###
		#################################################

} // end of class
?>