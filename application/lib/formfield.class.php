<?php

namespace skf;

/**
 * Creates a form object
 *
 */
class formField extends \domDocument
{
	/**
	 * Constructor, Creates an form element
	 * @param	string	$element
	 *
	 */
	public function __construct( string $element, $value=null )
	{
		parent::__construct( $element, $value );
		$this->formatOutput = true;
		$field = $this->createElement( $element, $value );
		$this->appendChild( $field );
	}

	/**
	 * Settor
	 * @param	string	$name
	 * @param	mixed	$value
	 * @return	void
	 */
	public function __set( $name, $value )
	{
		// because php variable must not have a minus sign we use underscores
		// and replace them here... yeah, it's a hack,  but whaddaya do?
		$name = str_replace( 'aria_', 'aria-', $name );
		$att = $this->createAttribute( $name );
		$att->value = $value;
		$this->documentElement->appendChild( $att );
	}

	/**
	 * Gettor
	 *
	 * @access	public
	 * @param	string	$name
	 * @return	string
	 *
	 */
	public function __gett( string $name ):string
	{
		return $this->$name;
	}

	public function addField( \domDocument $field )
	{
		$field = $this->importNode( $field->documentElement, true );
		$this->documentElement->appendChild( $field );
	}

	/**
	 * Returns a string representation of the class
	 *
	 * @access	public
	 * @return	string
	 *
	 */
	public function __toString():string
	{
		return $this->saveHTML();
	}
}
