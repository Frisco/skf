<?php

namespace skf;

class migrations{

	public $errors = [];
	
	public function __construct( dao_migrations $dao_migrations, Config $config, migrationsIterator $migrationsIterator )
	{
		$this->config = $config;
		$this->dao_migrations = $dao_migrations;
		$this->migrationsIterator = $migrationsIterator;
	}

	/**
	 * Returns an array of filenames yet to be migrated
	 *
	 * @access	public
	 * @return	array
	 *
	 */
	public function fetchPending():array
	{
		$ret = [];
		foreach( $this->migrationsIterator as $it )
		{
			// if the file has been loaded, skip
			if( $this->dao_migrations->isLoaded( $it->getPathname() ) == true )
			{
				$it->next();
			}
			else
			{
				$ret[] = $it->getFilename();
			}
		}
		return $ret;
	}

	/**
	 * Run the migrations. Returns an array of messages
	 *
	 * @access	public
	 * @return	array
	 *
	 */
	public function run():array
	{
		$messages = [];
		$migrations_dir = APP_PATH.$this->config->config_values['migrations']['dir'];

		foreach( $this->migrationsIterator as $item )
		{
			if( $this->dao_migrations->isLoaded( $item->getPathname() ) )
			{
				$messages[] = "Skipped: File ".$item->getPathname()." has already been loaded.";
			}
			else
			{
				if( $this->dao_migrations->isModified( $item->getPathname() ) )
				{
					$messages[] = "Skipped: File ".$item->getPathname()." has been modified since loading.";
				}
				else
				{
					$messages[] = $item->getPathname();
					$this->dao_migrations->loadMigration( $item->getPathname() );
				}
			}
		}
		try
		{
			// rebuild the DAOs
			$this->dao_migrations->generateDAO();
		}
		catch( \Exception $e )
		{
			$messages[] = $e->getMessage();
		}
		return $messages;
	}

} // end of class
