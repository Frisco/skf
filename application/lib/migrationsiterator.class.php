<?php

namespace skf;

class migrationsIterator extends \FilterIterator {

	/**
	 *
	 * Constructor, duh!
	 * @access	public
	 * @param	DirectoryIterator	$iterator
	 * @return	void
	 *
	 */
	public function __construct( Config $config )
	{
		$dir = $config->config_values['migrations']['dir'];
		parent::__construct( new \directoryIterator( APP_PATH.$dir ) );
	}

	/**
	 * Only files which are not loaded will be accepted
	 * @return bool (false if file has already been loaded, true otherwise
	 *
	 */
	public function accept() {
		// filter out dot files
		if( $this->isDot() || substr( $this->current(), 0, 1 ) == '.' )
		{
			return false;
		}
		return true;
	}


} // end of class 
