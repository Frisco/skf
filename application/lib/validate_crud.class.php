<?php

namespace skf;

class validate_crud extends \skf\validation{

        public function loadRules()
        {
                $this->addValidator( array( 'name'=>'table_name', 'type'=>'string', 'required'=>true, 'min'=>1, 'max'=>40, 'trim'=>1 ) );
                $this->addValidator( array( 'name'=>'existing_controller', 'type'=>'string', 'required'=>false, 'min'=>2, 'max'=>1500, 'trim'=>1 ) );
        }

} // end of class

