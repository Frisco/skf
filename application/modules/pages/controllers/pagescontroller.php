<?php
/**
 * File containing the pages controller
 *
 * @package SKF
 * @copyright Copyright (C) 2019 PHPRO.ORG. All rights reserved.
 *
 */

namespace skf;

class pagesController extends baseController implements IController
{

	public function __construct( View $view, Uri $uri, Config $config )
	{
		parent::__construct( $view, $uri );
		// from the  config
		$this->view->current_menu_item = 'about';
		$this->view->version = $config->config_values['application']['version'];
	}


	public function callIndex()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Pages';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/shop.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'shop.phtml', $cache_id);
	}

	public function callNews()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'News';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/news.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'news.phtml', $cache_id);
	}


	public function callPortfolio()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Portfolio';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/portfolio.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'portfolio.phtml', $cache_id);
	}


	public function callProduct()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Product';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/product.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'product.phtml', $cache_id);
	}


	public function callMarketing()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Marketing';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/marketing.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'marketing.phtml', $cache_id);
	}


	public function callAgency()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Agency';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/agency.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'agency.phtml', $cache_id);
	}


	public function callSlider()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Slider';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/slider.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'slider.phtml', $cache_id);
	}

	public function callRealestate()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'RealEstate';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/realestate.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'realestate.phtml', $cache_id);
	}


	public function callShop()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/pages/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/pages/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Shop';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'pages/shop.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'shop.phtml', $cache_id);
	}

}
