<?php
/**
 * File containing the blog controller
 *
 * @package SKF
 * @copyright Copyright (C) 2009 PHPRO.ORG. All rights reserved.
 *
 */

namespace skf;

class blogController extends baseController implements IController
{

	public function __construct( View $view, Uri $uri, Config $config )
	{
		parent::__construct( $view, $uri );
		// from the  config
		$this->view->version = $config->config_values['application']['version'];
	}

	public function callIndex()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/blog/views');

		/*** a view variable ***/
		$this->view->title = 'SKF Framework Simple Blog';

		$this->view->current_menu_item = 'blog';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'blog/index.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'index.phtml', $cache_id);
	}

	public function callView()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/blog/views');

		/*** a view variable ***/
		$this->view->title = 'Pete Townsend Arrested';

		$this->view->current_menu_item = 'Blog';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'blog/view.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'view.phtml', $cache_id);
	}
} // end of class
