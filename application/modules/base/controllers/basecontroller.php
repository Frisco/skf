<?php

namespace skf;

class baseController
{
	protected $view, $content=null;

	public function __construct( View $view, Uri $uri )
	{
		$this->uri = $uri;
		$this->view = $view;
		
		// javascript loader
		$fragment = $uri->fragment(0);
		$module = empty( $fragment ) ? 'index' : $uri->fragment(0);
		// did somebody try to pass a dodgey controller value in the url?
		$path = APP_PATH."/modules/$module/assets/js";
		if( is_dir( $path ) )
		{
			$js_loader = new asset_loader( APP_PATH."/modules/$module/assets/js" );
			$this->view->javascript = $js_loader;
		}
		else
		{
			$this->view->javascript = '';
		}

		// css loader
		$module = empty( $fragment ) ? 'index' : $uri->fragment(0);
		// did somebody try to pass a dodgey controller value in the url?
		$path = APP_PATH."/modules/$module/assets/css";
		if( is_dir( $path ) )
		{
			$css_loader = new asset_loader( APP_PATH."/modules/$module/assets/css" );
			$this->view->css = $css_loader;
		}       
		else    
		{
			$this->view->css = '';
		}
	}

	/**
	 * Set the view contents from the view template
	 * Set the page template, which will contain the view
	 *
	 * @access	public
	 *
	 */
        public function setPage()
	{
		$this->view->content = $this->content;
		$this->page = $this->view->fetch( APP_PATH.'/'.$this->view->layout_dir.'/'.$this->view->layout_file );
	}

	/**
	 * Get the page contents
	 *
	 * @access	public
	 * @return	string
	 *
	 */
	public function getPage()
	{
		return $this->page;
	}

}
