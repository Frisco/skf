<?php
/**
 * File containing the crud controller
 *
 * @package SKF
 * @copyright Copyright (C) 2019 PHPRO.ORG. All rights reserved.
 *
 */

namespace skf;

class crudController extends baseController implements IController
{

	public function __construct( View $view, Uri $uri, Config $config, Db $db, Migrations $migrations )
	{
		parent::__construct( $view, $uri );
		// a new config
		$this->view->version = $config->config_values['application']['version'];
		$this->db = $db;
		$this->migrations = $migrations;
	}

	public function callIndex()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/crud/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/crud/index.phtml';

		// $mig = (new Resolver)->resolve( 'skf\migrationsIterator' );
		$tpl->pending = $this->migrations->fetchPending();
		

		/*** a view variable ***/
		$this->view->title = 'Crud';
		// if the table name is set let's party
		if( isset( $_POST['table_name'] ) )
		{
			$val = new validate_crud;
			$val->source = $_POST;
			$val->loadRules();
			$val->run();
			if( $val->isValid() )
			{
				$table_name = $val->sanitized['table_name'];
				$existing_controller = $val->sanitized['existing_controller'];

				// run migrations
				$this->migrations->run();

				// regenerate DAOs
				$dao = new \skf\dao( $this->db, null );
				$dao->generateDAO();

				// check if module exists
				$controller_name = $table_name.'Controller';
				$module_path = APP_PATH.'/modules/'.$table_name; 

				// set up the crud
				$crud = (new Resolver)->resolve( 'skf\\crud' );
				if( strlen( $existing_controller ) > 0 )
				{
					// write to existing controller
					$crud->create( $table_name, $existing_controller );
				}
				elseif( is_dir( $module_path ) )
				{
					// this module/controller already exists, write to it.
					$crud->create( $table_name, $controller_name );
				}
				else
				{
					// create controller and write to new module controller
					// new module will be created with CRUD
					$crud->create( $table_name );
				}

				if( $crud->isValid() )
				{
					echo 'joy';
				}
				else
				{
					echo $crud->errorList();
				}
			}
			else
			{
				$tpl->message = $val->makeErrorList();
			}
		}

		$tpl->form = $this->createForm( $this->listTables() );

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'crud/index.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'index.phtml', $cache_id);
	}


	public function listTables()
	{
		$sql = 'SHOW TABLES';
		$query = $this->db->query($sql);
		return $query->fetchAll( \PDO::FETCH_COLUMN );
	}

	/**
	 * Create the form with the table names dropdown
	 *
	 * @access	public
	 * @param	array	$tables
	 * @return	object	form object
	 *
	 */
	public function createForm( array $tables ): form 
	{
		// $form = (new Resolver)->resolve( 'skf\\form' );
		$form = new form;
		$form->create();
		$form->id ='dao_form';
		$form->class ='form';
		$form->method = 'post';
		$form->action = '/crud';

		// create a fieldset
		$fs1 = new formField( 'fieldset' );
		$fs1->id = 'fs1';
		// create legend
		$legend = new formField( 'legend', 'Select Database Table' );
		$legend->for = 'table_name';
		// add the legend to the fieldset
		$fs1->addField( $legend );

		// create the dropdown
		$options = new formField( 'select' );
		$options->id = 'table_name';
		$options->title = 'Table name must be alpha numeric and underscore only';
		$options->maxlength = 3;
		$options->minlength = 15;
		$options->name = 'table_name';
		$options->autofocus = 'true'; 
		$options->required = 'required'; 

		foreach( $tables as $t )
		{
			$opt = new formField( 'option', $t);
			$opt->id = $t;
			$opt->value = $t;
			$options->addField( $opt );
		}
		$fs1->addField( $options );

		// create legend
		$legend = new formField( 'legend', 'Enter Existing Controller (Optional)' );
		$legend->for = 'existing_controller';
		// add the legend to the fieldset
		$fs1->addField( $legend );

		// create an input field which is required
		$field = new formField( 'input' );
		$field->type = 'text';
		// $field->required = 'required';
		$field->name = 'existing_controller';
		$field->title = 'Table name must be alpha numeric and underscore only';
		$field->min = 3;
		$field->max = 15;

		$field->id = 'existing_controller';
		// add the input field to the fieldset
		$fs1->addField( $field );

		// create the submit button
		$submit = new formField( 'input' );
		$submit->type="submit";
		$submit->value="Create Crud";
		$submit->id="submitButton";
		// add the fieldset to the form, boom!
		$fs1->addField( $submit );

		$form->addField( $fs1 );

		return $form; 
	}

} // end of class
