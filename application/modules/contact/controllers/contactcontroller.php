<?php
/**
 * File containing the contact controller
 *
 * @package SKF
 * @copyright Copyright (C) 2009 PHPRO.ORG. All rights reserved.
 *
 */

namespace skf;

class contactController extends baseController implements IController
{

	public function __construct( View $view, Uri $uri, Config $config )
	{
		parent::__construct( $view, $uri );
		// from the  config
		$this->view->version = $config->config_values['application']['version'];
	}

	public function callIndex()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/contact/views');

		/*** the include template ***/
		$tpl->include_tpl = APP_PATH . '/views/contact/index.phtml';

		$this->view->current_menu_item = 'contact';

		// set up the validators
		$val = new validate_contact;
		$val->loadRules();
		foreach( $val->validators as $form_field )
		{
			// eg: email_aria_invalid
			$aria_invalid = $form_field[0]['name'].'_aria_invalid';
			$tpl->$aria_invalid = 'true';
		}

		$tpl->errors = '';

		// was the form submitted?
		if( isset( $_POST['name'], $_POST['email'], $_POST['subject'], $_POST['message'] ) )
		{
			$val = new validate_contact();
			$val->source = $_POST;
			$val->loadRules();
			$val->run();

			if( $val->isValid() )
			{
	        		echo "done\n";
			}
			else
			{
				foreach( $val->errors as $form_field=>$err )
				{
					$aria_invalid = $form_field.'_aria_invalid';
					$tpl->$aria_invalid = 'false';
				}
				$tpl->errors = $val->makeErrorList();
			}
		}

		/*** a view variable ***/
		$this->view->title = 'Contact';

		$form = new form;
		$form->id = 'contact_form';
		$form->action = PUBLIC_PATH.'/contact';
		$form->method = 'post';
		$form->class = 'contact-us';

		$fieldset = new formField( 'fieldset' );

		$legend = new formField( 'legend', 'Please contact us' );
		$fieldset->addField( $legend );

		// label for name field
		$label = new formField( 'label', 'Please enter your name' );
		$label->for = 'name';
		$fieldset->addField( $label );

		// the name field
		$name = new formField( 'input' );
		$name->type = 'text';
		$name->id = 'name';
		$name->name = 'name';
		$name->min = 3;
		$name->max = 25;
		$name->aria_required = 'true';
	        $name->aria_invalid = 'false';
		$name->required = true;
		$fieldset->addField( $name );

		// label for name field
		$label = new formField( 'label', 'Please enter email address' );
		$label->for = 'email';
		$fieldset->addField( $label );

		// the email field
		$email = new formField( 'input' );
		$email->type = 'email';
		$email->id = 'email';
		$email->name = 'email';
		$email->min = 3;
		$email->max = 25;
		$email->aria_required = 'true';
	        $email->aria_invalid = 'false';
		$email->required = true;
		$fieldset->addField( $email );

		// label for subject field
		$label = new formField( 'label', 'Please enter a Subject' );
		$label->for = 'subject';
		$fieldset->addField( $label );

		// the email field
		$subject = new formField( 'input' );
		$subject->type = 'text';
		$subject->id = 'subject';
		$subject->name = 'subject';
		$subject->min = 3;
		$subject->max = 100;
		$subject->aria_required = 'true';
	        $subject->aria_invalid = 'false';
		$subject->required = true;
		$fieldset->addField( $subject );

	
		// label for message field
		$label = new formField( 'label', 'Please enter a short message' );
		$label->for = 'message';
		$fieldset->addField( $label );

		// the message field
		$message = new formField( 'textarea' );
		$message->id = 'message';
		$message->name = 'message';
		$message->min = 30;
		$message->max = 1000;
		$message->rows = 12;
		$message->aria_required = 'true';
	        $message->aria_invalid = 'false';
		$message->required = true;
		$fieldset->addField( $message );
	
		// label for submit button 
		$label = new formField( 'label', 'Click to send enquiry' );
		$label->for = 'sendButton';
		$fieldset->addField( $label );

		// the message field
		$button = new formField( 'input' );
		$button->type = 'button';
		$button->id = 'sendButton';
		$button->name = 'sendButton';
		$button->value = 'Submit Message';
		$button->onClick = "alert('Thanks for clicking');";
		$fieldset->addField( $button );

		// add the fieldset, which contains the form fields, to the form
		$form->addField( $fieldset );

		// send the form object to the template
		$tpl->contact_form = $form;

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'contact/index.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'index.phtml', $cache_id);
	}
}
