<?php
/**
 * File containing the admin controller
 *
 * @package SKF
 * @copyright Copyright (C) 2019 PHPRO.ORG. All rights reserved.
 *
 */

namespace skf;

class adminController extends baseController implements IController
{

	public $db;

	public function __construct( View $view, Uri $uri, Config $config, Db $db, Migrations $migrations )
	{
		parent::__construct( $view, $uri );
		// from the  config
		$this->view->version = $config->config_values['application']['version'];
		$this->db = $db;
		$this->config = $config;
		$this->migrations = $migrations;
	}

	public function callIndex()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/admin/views');

		$this->view->layout_dir = '/modules/admin/layouts';

		/*** layout template ***/
		$this->view->layout_file = 'index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Admin';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'admin/index.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'index.phtml', $cache_id);
	}


	public function callMigrations()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/admin/views');

		$this->view->layout_dir = '/modules/admin/layouts';

		/*** layout template ***/
		$this->view->layout_file = 'index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Admin - Migrations';

		$dao = new dao_migrations( $this->db );

		// fetch a list of pending migrations
		$pending = $this->migrations->fetchPending();
		$tpl->migration_files = $pending;

		$tpl->messages = ['Migrations'];
		if( isset( $_POST['run_migrations'] ) )
		{
			$tpl->messages = $this->migrations->run();
		}

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'admin/migrations.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'migrations.phtml', $cache_id);
	}

	public function callBuildDao()
	{
		/*** layout template ***/
		$this->view->layout_file = 'admin_layout.phtml';

		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		// $view->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/admin/views');

		$this->view->layout_dir = '/modules/admin/layouts';

		/*** layout template ***/
		$this->view->layout_file = 'index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Admin';

		$form = (new Resolver)->resolve( 'skf\form' );
		$form->create();
		$form->id ='dao_form';
		$form->class ='form';
		$form->method = 'post';
		$form->action = '/admin/buildDAO';

		// create a fieldset
		$fs1 = new formField( 'fieldset' );
		$fs1->id = 'fs1';
		// create legend
		$legend = new formField( 'legend', 'Table name is optional' );

		// create an input field
		$field = new formField( 'input' );
		$field->type = 'text';
		$field->name = 'table_name';
		$field->id = 'table_name';

		// create the submit button
		$submit = new formField( 'input' );
		$submit->type="submit";
		$submit->value="Build";
		$submit->class="button";
		$submit->id="submitButton";

		// was the form posted ??
		if( isset( $_POST['table_name'] ) )
		{
			$val = new validate_builddao;
			$val->source = $_POST;
			$val->loadRules();
			$val->run();
			if( $val->isValid() )
			{
				$tpl->message = 'All DAOs successully updated';
				try
				{
					$dao = (new Resolver)->resolve( 'skf\dao' );
					$dao = new dao( $this->db );
					$dao->generateDAO();
				}
				catch( \Exception $e )
				{
					// add exceptions to the validation errors
					$tpl->warning = isset( $warning ) ? $warning : '';
				}
			}
			else
			{
				$tpl->message = $val->makeErrorList();
			}
		}
		else
		{
			// form was not posted
		}


		// add the legend to the fieldset
		$fs1->addField( $legend );

		// add the input field to the fieldset
		$fs1->addField( $field );

		// add the submit button to fieldset 2
		$fs1->addField( $submit );

		// add the fieldset to the form, boom!
		$form->addField( $fs1 );

		// add the form to the template
		$tpl->form = $form;

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'admin/builddao.php' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'builddao.phtml', $cache_id);
	}
}
